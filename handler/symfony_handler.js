define(function(require, exports, module) {

    var _ = require("../node_modules/lodash/index.js");
    var async = require("../node_modules/async/lib/async");
    var worker_util = require("plugins/c9.ide.language/worker_util");
    var baseHandler = require('plugins/c9.ide.language/base_handler');
    var handler = module.exports = Object.create(baseHandler);
    var PHP = require("../node_modules/ace/lib/ace/mode/php/php").PHP;
    
  debugger;
    var xml2js = require('./xml2js');
    //var xml2js = require('../node_modules/xml2js/lib/xml2js');

    var tokenTypes = {};
    for (var key in PHP.Constants) {
        tokenTypes[PHP.Constants[key]] = key;
    }

    var settings = {
        symfony_console: "app/console"
    };

    /*handler.onmessage = function (oEvent) {
        debugger;
        settings = _.merge(settings, oEvent.data);
    };*/

    handler.container = {};
    handler.routes = {};

    handler.completions = [];

    handler.init = function(callback) {
        async.parallel([
            function(async_callback) {
                getContainer(function(container) {
                    handler.container = container;
                    async_callback();
                });
            },
            function(async_callback) {
                getRoutes(function(routes) {
                    handler.routes = routes;
                    async_callback();
                });
            }
        ], function( /*err, results*/ ) {
            initCompletions();
            callback();
        });
    }

    handler.handlesLanguage = function(language) {
        return language === "php";
    };

    handler.analyze = function(value, ast, callback) {
        if (!ast) {
            return;
        }
        callback([
            /*{
                        pos: { sl: 0, el: 0, sc: 0, ec: 0 },
                        type: "info",
                        level: "info",
                        message: "Hey there! I'm an info marker"
                    }*/
        ]);
    };

    handler.complete = function(doc, fullAst, pos, currentNode, callback) {
        debugger;
        /*var completions = _.where(handler.completions, function(c) {
            return c.name.indexOf(doc.getLine(pos.row)) == 0;
        });
        callback(handler.completions);*/

        if (doc.language != "php") {
            callback([]);
            return;
        }

        /*var currentLineRegex = new RegExp(/^(\s*)(\S+.*)$/);
        var currentLineMatch = currentLineRegex.exec(doc.getLine(pos.row));
        var currentLine = currentLineMatch[2];
        var realPos = currentLineMatch[1].length;*/
        var realPos = pos.column;

        // worker_util.getTokens(doc, null, function(err, result) {}); // <- Bullshit

        var value = "<?php " + doc.getValue();
        var tokens = PHP.Lexer(value, {
            short_open_tag: 0
        });

        var currentLineTokens = _.filter(tokens, function(token) {
            return Array.isArray(token) ? (token[2] == pos.row + 1) : false;
        });
        var offset = 0,
            currentToken = null;

        for (var i = 0; i < currentLineTokens.length; i++) {
            var token = currentLineTokens[i],
                nextOffset = offset + (Array.isArray(token) ? token[1].length : token.length);
            if (realPos >= offset && realPos <= nextOffset) {
                currentToken = token;
                break;
            }
            offset = nextOffset;
        }

        if (currentToken) {
            currentToken = {
                type: tokenTypes[currentToken[0]],
                value: currentToken[1],
                line: currentToken[2]
            };
        }

        if (!currentToken || currentToken.type != "T_CONSTANT_ENCAPSED_STRING") {
            //callback([]);
            //return;
        }

        var regex = new RegExp(/[=;:\[\]{}&|!@%*/+(),.\"'`<>\-\s]*(\w+)$/),
            symbol = doc.getLine(pos.row).substring(0, pos.column - 1),
            match = regex.exec(symbol),
            filter = match.length == 2 ? match[1] : "";

        if (handler.completions.length == 0) {
            async.parallel([
                function(async_callback) {
                    getContainer(function(container) {
                        handler.container = container;
                        async_callback();
                    });
                },
                function(async_callback) {
                    getRoutes(function(routes) {
                        handler.routes = routes;
                        async_callback();
                    });
                }
            ], function( /*err, results*/ ) {
                initCompletions();
                callback(filterCompletions(handler.completions, filter));
            });
        }
        else {
            callback(filterCompletions(handler.completions, filter));
        }


        /*callback([{
            name        : "foo()",
            replaceText : "foo()",
            icon        : "method",
            meta        : "FooClass",
            doc         : "The foo() method",
            docHead     : "FooClass.foo",
            priority    : 1
        }]);*/
    };

    function filterCompletions(completions, filter) {
        return _.filter(completions, function(completion) {
            var idx = completion.name.indexOf(filter);
            if (idx == 0) {
                completion.priority = 10;
            }
            else if (idx >= 0) {
                //debugger;
                completion.priority = 1;
                completion.exactMatch = 0;
                completion.matchMask = 0 | (1 << idx);
            }
            return idx >= 0;
        });
    }

    function initCompletions() {
        var parameters = handler.container.parameters,
            services = handler.container.services,
            routes = handler.routes;
        var parameter, service, route, completion;

        handler.completions = [];

        /*for (parameter in parameters) {
            if (parameters.hasOwnProperty(parameter)) {
                completion = {
                    name        : parameter,
                    replaceText : parameter,
                    isContextual: true,
                    icon        : "parameter",
                    meta        : "SymfonyContainerParameter",
                    doc         : "The " + parameter +" = '" + parameters[parameter] + "' symfony container parameter",
                    docHead     : parameter,
                    priority    : 1
                };
                handler.completions.push(completion);
            }
        }*/

        for (service in services) {
            if (services.hasOwnProperty(service)) {
                completion = {
                    name: service,
                    replaceText: service,
                    icon: "service",
                    meta: "SymfonyContainerService",
                    doc: services[service].class,
                    docHead: service,
                    priority: 1
                };
                handler.completions.push(completion);
            }
        }

        /*for (route in routes) {
            if (routes.hasOwnProperty(route)) {
                completion = {
                    name        : route,
                    replaceText : route,
                    isContextual: true,
                    icon        : "route",
                    meta        : "SymfonyRoute",
                    doc         : routes[route].controller,
                    docHead     : routes[route].path,
                    priority    : 1
                };
                handler.completions.push(completion);
            }
        }*/
    }

    function runSymfonyCommand(command, args, callback) {
        var result = "";
        var workspaceDir = handler.workspaceDir;

        worker_util.execFile("php", {
            args: [(workspaceDir + "/" + settings.symfony_console), command].concat(args)
        }, function(err, stdout, stderr) {
            if (!err) {
                result = stdout;
                callback(result);
            }
            else {
                console.log(err);
            }
        });
    }
    
    /**
     * @return {
     *     services: services,
     *     parameters: parameters
     * }
     */
    function getContainer/*ByXML*/(callback) {
        var services = {},
            parameters = {};
            
        var handleErr = function (err) { 
            if (err) { 
                callback({ services: {},  parameters: {} });
            }
        };
        
        worker_util.readFile("app/cache/dev/appDevDebugProjectContainer.xml", {allowUnsaved: false}, function(err, xml) {
            handleErr(err);
            
            var parser = new xml2js.Parser();
            parser.parseString(xml, function (err, result) {
                handleErr(err);
                console.log(result);
                debugger;
            });
        });
    }

    function getContainerByCommand(callback) {
        var services = {},
            parameters = {},
            match, alias, alias_match;

        async.series([
            function(async_callback) {
                runSymfonyCommand("debug:container", ["--show-private"], function(result) {

                    var nl_1 = result.indexOf("\n");
                    var nl_2 = result.indexOf("\n", nl_1 + 1);
                    result = result.substr(nl_2 + 1);

                    // fragment.renderer.ssi                                              Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer                                  
                    // kernel                                                                                                                                                        
                    // mailer                                                             alias for "swiftmailer.mailer.default"     
                    var regex = /([\w._-]+)(\s+([\w ."'\\]+)){0,1}\n/g;
                    while ((match = regex.exec(result))) {

                        // Ignore bullshit like
                        // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_1   Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplatePathsCacheWarmer
                        // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_10  Symfony\Component\HttpFoundation\Session\Flash\FlashBag
                        // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_100 Symfony\Bundle\AsseticBundle\Factory\Resource\DirectoryResource
                        if ((/^[a-f\d]{64}_\d+/).test(match[1])) {
                            continue;
                        }

                        if (match[3]) {
                            alias_match = (/alias for "(.+)"/).exec(match[3]);
                            if (alias_match) {
                                alias = alias_match[1];
                            }
                            else {
                                alias = null;
                            }
                        }
                        else {
                            alias = null;
                        }

                        services[match[1]] = {
                            "class": alias ? null : match[3].trim(),
                            "alias": alias ? alias.trim() : null
                        };
                    }

                    for (var key in services) {
                        if (services[key].alias) {
                            services[key].class = services[services[key].alias].class;
                        }
                    }
                    async_callback();
                });
            },
            function(async_callback) {
                runSymfonyCommand("debug:container", ["--parameters"], function(result) {
                    // Parameters
                    var nl_1 = result.indexOf("\n");
                    var nl_2 = result.indexOf("\n", nl_1 + 1);
                    result = result.substr(nl_2 + 1);

                    // validator.email.class                                                 Symfony\Component\Validator\Constraints\EmailValidator                                         
                    // validator.expression.class                                            Symfony\Component\Validator\Constraints\ExpressionValidator                                    
                    // validator.mapping.cache.prefix                                                   
                    var regex = /([\w._-]+)(\s+(.+)){0,1}\n/g;
                    while ((match = regex.exec(result))) {
                        parameters[match[1]] = match[3];
                    }
                    async_callback();
                });
            },
            function(async_callback) {
                callback({
                    services: services,
                    parameters: parameters
                });
                async_callback();
            }
        ]);
    }

    function getRoutes(callback) {
        var match, routes = {};
        runSymfonyCommand("debug:router", ["--show-controllers"], function(result) {
            var nl_1 = result.indexOf("\n");
            var nl_2 = result.indexOf("\n", nl_1 + 1);
            result = result.substr(nl_2 + 1);

            //  Name                     Method   Scheme Host Path 
            //  admin_post_new           GET|POST ANY    ANY  /admin/post/new                   
            //  admin_post_show          GET      ANY    ANY  /admin/post/{id}                  
            //  admin_post_edit          GET|POST ANY    ANY  /admin/post/{id}/edit     
            var regex = /([\w._-]+)\s+([\w|]+)\s+([\w|]+)\s+([\w|]+)\s+([\w .\{\}\/]+?)\s+([\w.:\\]+).*\n/g;
            while ((match = regex.exec(result))) {

                var controller_controller = match[6].substr(0, match[6].lastIndexOf(":"));
                var controller_action = match[6].substr(match[6].lastIndexOf(":"));

                routes[match[1]] = {
                    "controller": controller_controller,
                    "action": controller_action,
                    "path": match[5],
                    "method": match[2]
                };
            }

            callback(routes);
        });
    }

    function getTwigDirectives(callback) {
        runSymfonyCommand("debug:twig", [], callback);
    }

    function findNamespaces(callback) {
        var psr4_received = false,
            namespaces_received = false,
            psr4, namespaces;
        var parseAutoload = function(psr4, ns) {
            var content = psr4 + ns;

            // 'Symfony\\Bundle\\SwiftmailerBundle\\' => array($vendorDir . '/symfony/swiftmailer-bundle'),
            var regex = /'([\w\\]+)'\s*=>\s*array\s*\(\s*((\$[\w]+)\s*\.\s*)?'(.+?)'\)/g;

            var namespaces = [],
                match;
            while ((match = regex.exec(content))) {
                namespaces.push({
                    namespace: match[1].replace(/[\\]{2}/g, "\\"),
                    dir: match[3],
                    path: match[4],
                });
            }

            callback(namespaces);
        }

        worker_util.readFile("vendor/composer/autoload_psr4.php", {allowUnsaved: false}, function(err, data) {
            if (err) {
                psr4 = "";
            }
            else {
                psr4 = data;
            }
            psr4_received = true;
            if (namespaces_received) {
                parseAutoload(psr4, namespaces);
            }
        });
        worker_util.readFile("vendor/composer/autoload_namespaces.php", {allowUnsaved: false}, function(err, data) {
            if (err) {
                namespaces = "";
            }
            else {
                namespaces = data;
            }
            namespaces_received = true;
            if (psr4_received) {
                parseAutoload(psr4, namespaces);
            }
        });
    }

    function findClass(className, callback) {
        findNamespaces(function(autoloads) {
            var autoload, candidate, result_sent = false;
            var candidates = [];
            candidates.push(
                "src/" + className.replace(/[\\]/g, "\/") + ".php"
            );

            for (var i = 0; i < autoloads.length; i++) {
                autoload = autoloads[i];
                if (className.indexOf(autoload.namespace) === 0) {
                    var path;
                    switch (autoload.dir) {
                        case "$baseDir":
                            path = ".";
                            break;
                        case "$vendorDir":
                            path = "vendor";
                            break;
                        default:
                            path = ".";
                            break;
                    }
                    path += autoload.path + "/";
                    path += className /*.substr(autoload.namespace.length)*/ .replace(/[\\]/g, "\/") + ".php";

                    candidates.push(path);
                }
            }

            var not_checked = [];
            for (var i = 0; i < candidates.length; i++) {
                not_checked.push(candidates[i]);
            }

            for (var i = 0; i < candidates.length; i++) {
                if (result_sent) {
                    break;
                }
                candidate = candidates[i];
                (function(candidate) {
                    worker_util.stat(candidate, function(err, data) {
                        if (!err && data) {
                            not_checked.splice(not_checked.indexOf(candidate), 1);

                            if (!result_sent) {
                                result_sent = true;
                                callback(candidate, candidates);
                            }
                            else if (not_checked.length == 0) {
                                callback(null, candidates);
                            }
                        }
                    });
                })(candidate);
            }
        });
    }

    function findService(id) {

    }

    function findRoute(name) {

    }
});