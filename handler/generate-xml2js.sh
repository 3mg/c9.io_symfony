cat > xml2js.src.js <<EOF
var xml2js = require('xml2js');

// AMD / RequireJS
if (typeof define !== 'undefined' && define.amd) {
    define([], function () {
        return xml2js;
    });
}
// Node.js
else if (typeof module !== 'undefined' && module.exports) {
    module.exports = xml2js;
}
// included directly via <script> tag
else {
    this.xml2js = xml2js;
}
EOF

browserify xml2js.src.js -d --bare --no-commondir|sed 's/typeof require=="function"&&require/false/g' > xml2js.js

rm xml2js.src.js