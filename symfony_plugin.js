define(function(require, exports, module) {

    // Source: http://stackoverflow.com/questions/273789/is-there-a-version-of-javascripts-string-indexof-that-allows-for-regular-expr
    String.prototype.regexIndexOf = function(regex, startpos) {
        var indexOf = this.substring(startpos || 0).search(regex);
        return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
    }
    
    String.prototype.regexLastIndexOf = function(regex, startpos) {
        regex = (regex.global) ? regex : new RegExp(regex.source, "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : ""));
        if(typeof (startpos) == "undefined") {
            startpos = this.length;
        } else if(startpos < 0) {
            startpos = 0;
        }
        var stringToWorkWith = this.substring(0, startpos + 1);
        var lastIndexOf = -1;
        var nextStop = 0;
        var result;
        while((result = regex.exec(stringToWorkWith)) != null) {
            lastIndexOf = result.index;
            regex.lastIndex = ++nextStop;
        }
        return lastIndexOf;
    }
    
    main.consumes = ["Plugin", "commands", "tabManager", "bridge", "fs", "menus", "ui", "proc", "settings", "preferences", "Dialog", "dialog.alert", "dialog.question", "open", "language", "symfony_utils", "symfony_editor_utils"];
    main.provides = ["symfony_plugin"];
    return main;

    function main(options, imports, register) {
        var Plugin = imports.Plugin;
        var commands = imports.commands;
        var tabs = imports.tabManager;
        var settings = imports.settings;
        
        var ui = imports.ui;
        var prefs = imports.preferences;
        var menus = imports.menus;
        var alert = imports["dialog.alert"].show;
        var question = imports["dialog.question"].show;
        var Dialog = imports.Dialog;
        
        //var bridge = imports.bridge;
        //var client = imports["bridge.client"];
        
        var fs = imports.fs;
        var proc = imports.proc;
        var open = imports.open;
        var async = require("async");
        
        var language = imports.language;
        
        
        (function () {
            //debugger;
            language.registerLanguageHandler('plugins/symfony.plugin/handler/symfony_handler');
        })();
        
        var symfony_utils = imports.symfony_utils;
        var symfony_editor_utils = imports.symfony_editor_utils;

        /***** Initialization *****/
        
        var plugin = new Plugin("Ajax.org", main.consumes);
        var emit = plugin.getEmitter();
        
        var findClassDialog = new Dialog("Ajax.org", main.consumes, {
            name: "dialog.find_class",
            allowClose: true,
            dark: true,
            title: "Find class",
            width: 400,
            zindex: 100000,
            modal: true,
            elements: [
                { type: "filler" },
                { type: "button", id: "cancel", caption: "Cancel", onclick: cancelFindClass },
                { type: "button", id: "find", color: "green", caption: "Find", onclick: doFindClass }
            ]
        });
        var findClassDialogEmit = findClassDialog.getEmitter();
        
        findClassDialog.on("draw", function(options){
            ui.insertMarkup(options.aml, require("text!./findClass.xml"), findClassDialog);
        });
        
        function load() {
            commands.addCommand({
                name: "align_selection",
                bindKey: { 
                    mac: "Command-Shift-K", 
                    win: "Ctrl-Shift-K" 
                },
                exec: function(editor){ 
                    console.log(arguments);
                    //debugger;
                    symfony_editor_utils.alignSelection(editor.ace);
                },
                isAvailable: function(editor) {
                    if (editor && editor.ace)
                        return !editor.ace.selection.isEmpty();
                    return false;
                }
            }, plugin);
            
            commands.addCommand({
                name: "sf_get_container",
                exec: function(editor){ 
                    var container = "";
                    symfony_utils.getContainer(function(container){
                        console.log(container);
                    });
                },
                isAvailable: function() {
                    return true;
                }
            }, plugin);
            
            commands.addCommand({
                name: "sf_get_routes",
                exec: function(editor){ 
                    symfony_utils.getRoutes(function(routes){
                        console.log(routes);
                    });
                },
                isAvailable: function() {
                    return true;
                }
            }, plugin);
            
            commands.addCommand({
                name: "sf_find_psr_roots",
                exec: function(editor){ 
                    symfony_utils.findNamespaces(function(namespaces){
                        console.log(namespaces);
                    });
                },
                isAvailable: function() {
                    return true;
                }
            }, plugin);
            
            commands.addCommand({
                name: "sf_find_class",
                exec: function(editor){
                    var el;
                    if (findClassDialog.aml) {
                        el = findClassDialog.getElement("className");
                        if (el) {
                            el.setValue("");
                        }
                    }
                    findClassDialog.show();
                },
                isAvailable: function() {
                    return true;
                }
            }, plugin);
            
            settings.on("read", function(){
                settings.setDefaults("user/symfony", [["console", "workspace/app/console"]]);
            }, plugin);
            
            prefs.add({
                "Editors" : {
                    position: 10,
                    "Symfony" : {
                        "Console" : {
                            type: "textbox",
                            path: "user/symfony/@console",
                            position: 100
                        }
                    }
                }
            }, plugin);
            
            menus.setRootMenu("Symfony", 800, plugin);
            
            menus.addItemByPath("Symfony/Get container", new ui.item({
                command: "sf_get_container"
            }), 1000, plugin);
            
            menus.addItemByPath("Symfony/Get routes", new ui.item({
                command: "sf_get_routes"
            }), 1000, plugin);
            
            menus.addItemByPath("Symfony/Find psr roots", new ui.item({
                command: "sf_find_psr_roots"
            }), 1000, plugin);
            
            menus.addItemByPath("Symfony/Find class", new ui.item({
                command: "sf_find_class"
            }), 1000, plugin);
        }

        /***** Methods *****/
        
        function getPhpScope (ace){
            debugger;
            var range = ace.selection.getRange();
            var firstLine = ace.session.doc.getLine(range.start.row),
                lastLine;
                
            if(firstLine.trim() != ""){
                range.start.column = firstLine.regexIndexOf("\\S");
            } else {
                range.start.column = 0;
            }
            
            if(range.isMultiLine()){
                lastLine = ace.session.doc.getLine(range.end.row);
                if(lastLine.trim() == ""){
                    range.end.column = 0;
                } else {
                    range.end.column = lastLine.length;
                }
            } else {
                range.end.column = firstLine.length;
            }
            ace.selection.setRange(range);
            return range;
        }
        
        // Symfony
        
        function cancelFindClass() {
            findClassDialog.hide();
        }
        
        function doFindClass() {
            var className = findClassDialog.getElement("className").getValue();
            
            // AppBundle\Controller\BlogController
            // Doctrine\ORM\EntityManager
            
            symfony_utils.findClass(className, function(path, candidates){
                console.log([path, candidates]);
                if (path) {
                    tabs.openFile(path, true, function(){});
                }
            });
            
            findClassDialog.hide();
        }
        
        function openClass() {
            
        }
        
        /***** Lifecycle *****/
        
        plugin.on("load", function() {
            load();
        });
        plugin.on("unload", function() {

        });
        
        /***** Register and define API *****/
        
        plugin.freezePublicAPI({
            test: function(){
                console.log("test");
            }
        });
        
        register(null, {
            "symfony_plugin": plugin
        });
    }
});