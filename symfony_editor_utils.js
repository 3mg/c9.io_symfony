define(function(require, exports, module) {

    // Source: http://stackoverflow.com/questions/273789/is-there-a-version-of-javascripts-string-indexof-that-allows-for-regular-expr
    String.prototype.regexIndexOf = function(regex, startpos) {
        var indexOf = this.substring(startpos || 0).search(regex);
        return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
    }
    
    String.prototype.regexLastIndexOf = function(regex, startpos) {
        regex = (regex.global) ? regex : new RegExp(regex.source, "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : ""));
        if(typeof (startpos) == "undefined") {
            startpos = this.length;
        } else if(startpos < 0) {
            startpos = 0;
        }
        var stringToWorkWith = this.substring(0, startpos + 1);
        var lastIndexOf = -1;
        var nextStop = 0;
        var result;
        while((result = regex.exec(stringToWorkWith)) != null) {
            lastIndexOf = result.index;
            regex.lastIndex = ++nextStop;
        }
        return lastIndexOf;
    }
    
    main.consumes = ["Plugin"];
    main.provides = ["symfony_editor_utils"];
    return main;

    function main(options, imports, register) {
        var Plugin = imports.Plugin;
        
        /***** Initialization *****/
        
        var plugin = new Plugin("Ajax.org", main.consumes);
        var emit = plugin.getEmitter();

        function load() {
            
        }

        /***** Methods *****/
        
        // Ace
        
        function alignSelection (ace) {
            var range = ace.selection.getRange();
            var firstLine = ace.session.doc.getLine(range.start.row),
                lastLine;
                
            if(firstLine.trim() != ""){
                range.start.column = firstLine.regexIndexOf("\\S");
            } else {
                range.start.column = 0;
            }
            
            if(range.isMultiLine()){
                lastLine = ace.session.doc.getLine(range.end.row);
                if(lastLine.trim() == ""){
                    range.end.column = 0;
                } else {
                    range.end.column = lastLine.length;
                }
            } else {
                range.end.column = firstLine.length;
            }
            ace.selection.setRange(range);
            return range;
        }
        
        /***** Lifecycle *****/
        
        plugin.on("load", function() {
            load();
        });
        plugin.on("unload", function() {

        });
        
        /***** Register and define API *****/
        
        plugin.freezePublicAPI({
            alignSelection: alignSelection
        });
        
        register(null, {
            "symfony_editor_utils": plugin
        });
    }
});