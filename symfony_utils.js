define(function(require, exports, module) {

    // Source: http://stackoverflow.com/questions/273789/is-there-a-version-of-javascripts-string-indexof-that-allows-for-regular-expr
    String.prototype.regexIndexOf = function(regex, startpos) {
        var indexOf = this.substring(startpos || 0).search(regex);
        return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
    }
    
    String.prototype.regexLastIndexOf = function(regex, startpos) {
        regex = (regex.global) ? regex : new RegExp(regex.source, "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : ""));
        if(typeof (startpos) == "undefined") {
            startpos = this.length;
        } else if(startpos < 0) {
            startpos = 0;
        }
        var stringToWorkWith = this.substring(0, startpos + 1);
        var lastIndexOf = -1;
        var nextStop = 0;
        var result;
        while((result = regex.exec(stringToWorkWith)) != null) {
            lastIndexOf = result.index;
            regex.lastIndex = ++nextStop;
        }
        return lastIndexOf;
    }
    
    main.consumes = ["Plugin", "fs", "proc", "settings", "preferences"];
    main.provides = ["symfony_utils"];
    return main;

    function main(options, imports, register) {
        var Plugin = imports.Plugin;

        var settings = imports.settings;
        var prefs = imports.preferences;

        var fs = imports.fs;
        var proc = imports.proc;
        var open = imports.open;
        var async = require("async");

        /***** Initialization *****/
        
        var plugin = new Plugin("Ajax.org", main.consumes);
        var emit = plugin.getEmitter();

        
        function load() {
            
            settings.on("read", function(){
                settings.setDefaults("user/symfony", [["console", "workspace/app/console"]]);
            }, plugin);
            
            prefs.add({
                "Editors" : {
                    position: 10,
                    "Symfony" : {
                        "Console" : {
                            type: "textbox",
                            path: "user/symfony/@console",
                            position: 100
                        }
                    }
                }
            }, plugin);
        }

        /***** Methods *****/

        // Symfony
        
        function runSymfonyCommand(command, args, callback) {
            var result = "";
            proc.spawn("php", {
                args: [settings.get("user/symfony/@console"), command].concat(args)
            }, function(err, child) {
                if(!err){
                    child.stdout.on("data", function(data) {
                        result += data;
                    });
                    child.stdout.on("end", function() {
                        //console.log(result);
                        callback(result);
                    });
                }
            });
        }
        
        function getCommandsList(callback) {
            
        }
        
        function getContainer(callback) {
            var services = {}, parameters = {}, match, alias, alias_match;
            
            async.series([
                function(async_callback) {
                    runSymfonyCommand("debug:container", ["--show-private"], function(result) {
                        
                        var nl_1 = result.indexOf("\n");
                        var nl_2 = result.indexOf("\n", nl_1 + 1);
                        result = result.substr(nl_2 + 1);
                        
                        // fragment.renderer.ssi                                              Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer                                  
                        // kernel                                                                                                                                                        
                        // mailer                                                             alias for "swiftmailer.mailer.default"     
                        var regex = /([\w._-]+)(\s+([\w ."'\\]+)){0,1}\n/g;
                        while ( (match = regex.exec(result)) ) {
                            // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_1   Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplatePathsCacheWarmer
                            // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_10  Symfony\Component\HttpFoundation\Session\Flash\FlashBag
                            // d9e6cf86e809fce514675634e594bab869c1266199888d17e8f2b4ab9f6b39a1_100 Symfony\Bundle\AsseticBundle\Factory\Resource\DirectoryResource
                            // 
                            // Ignore bullshit
                            if ( (/^[a-f\d]{64}_\d+/).test(match[1]) ) {
                                continue;
                            }
                            
                            if (match[3]) {
                                alias_match = (/alias for "(.+)"/).exec(match[3]);
                                if(alias_match) {
                                    alias = alias_match[1];
                                } else {
                                    alias = null;
                                }
                            } else {
                                alias = null;
                            }
                            
                            services[match[1]] = {
                                "class": alias ? null : match[3].trim(),
                                "alias": alias ? alias.trim() : null
                            };
                        }
                        
                        for (var key in services) {
                            if (services[key].alias) {
                                services[key].class = services[services[key].alias].class;
                            }
                        }
                        async_callback();
                    });
                }, function(async_callback) {
                    runSymfonyCommand("debug:container", ["--parameters"], function(result) {
                        // Parameters
                        var nl_1 = result.indexOf("\n");
                        var nl_2 = result.indexOf("\n", nl_1 + 1);
                        result = result.substr(nl_2 + 1);
                        
                        // validator.email.class                                                 Symfony\Component\Validator\Constraints\EmailValidator                                         
                        // validator.expression.class                                            Symfony\Component\Validator\Constraints\ExpressionValidator                                    
                        // validator.mapping.cache.prefix                                                   
                        var regex = /([\w._-]+)(\s+(.+)){0,1}\n/g;
                        while ( (match = regex.exec(result)) ) {
                            parameters[match[1]] = match[3];
                        }
                        async_callback();
                    });
                }, function(async_callback) {
                    callback({
                        services: services,
                        parameters: parameters
                    });
                    async_callback();
                }
            ]);
        }
        
        function getRoutes(callback) {
            var match, routes = {};
            runSymfonyCommand("debug:router", ["--show-controllers"], function(result) {
                var nl_1 = result.indexOf("\n");
                var nl_2 = result.indexOf("\n", nl_1 + 1);
                result = result.substr(nl_2 + 1);
                
                //  Name                     Method   Scheme Host Path 
                //  admin_post_new           GET|POST ANY    ANY  /admin/post/new                   
                //  admin_post_show          GET      ANY    ANY  /admin/post/{id}                  
                //  admin_post_edit          GET|POST ANY    ANY  /admin/post/{id}/edit     
                var regex = /([\w._-]+)\s+([\w|]+)\s+([\w|]+)\s+([\w|]+)\s+([\w .\{\}\/]+?)\s+([\w.:\\]+).*\n/g;
                while ( (match = regex.exec(result)) ) {
                    
                    var controller_controller = match[6].substr(0, match[6].lastIndexOf(":"));
                    var controller_action = match[6].substr(match[6].lastIndexOf(":"));
                    
                    routes[match[1]] = {
                        "controller": controller_controller,
                        "action": controller_action,
                        "path": match[5],
                        "method": match[2]
                    };
                }
                    
                callback(routes);
            });
        }
        
        function getTwigDirectives(callback) {
            runSymfonyCommand("debug:twig", [], callback);
        }
        
        function findNamespaces(callback) {
            var psr4_received = false, namespaces_received = false, psr4, namespaces;
            var parseAutoload = function(psr4, ns) {
                var content = psr4 + ns;
                
                // 'Symfony\\Bundle\\SwiftmailerBundle\\' => array($vendorDir . '/symfony/swiftmailer-bundle'),
                var regex = /'([\w\\]+)'\s*=>\s*array\s*\(\s*((\$[\w]+)\s*\.\s*)?'(.+?)'\)/g;
                
                var namespaces = [], match;
                while ( (match = regex.exec(content)) ) {
                    namespaces.push({
                        namespace: match[1].replace(/[\\]{2}/g, "\\"),
                        dir: match[3],
                        path: match[4],
                    });
                }
                
                callback(namespaces);
            }
            
            fs.readFile("vendor/composer/autoload_psr4.php", function(err, data) {
                if (err) {
                    psr4 = "";
                } else {
                    psr4 = data;
                }
                psr4_received = true;
                if(namespaces_received){
                    parseAutoload(psr4, namespaces);
                }
            });
            fs.readFile("vendor/composer/autoload_namespaces.php", function(err, data){
                if (err) {
                    namespaces = "";
                } else {
                    namespaces = data;
                }
                namespaces_received = true;
                if(psr4_received){
                    parseAutoload(psr4, namespaces);
                }
            });
        }
        
        function findClass(className, callback) {
            findNamespaces(function(autoloads){
                var autoload, candidate, result_sent = false;
                var candidates = [];
                candidates.push(
                    "src/" + className.replace(/[\\]/g, "\/") + ".php"
                );
                
                for (var i = 0; i < autoloads.length; i++) {
                    autoload = autoloads[i];
                    if (className.indexOf(autoload.namespace) === 0) {
                        var path;
                        switch (autoload.dir) {
                            case "$baseDir":
                                path = ".";
                                break;
                            case "$vendorDir":
                                path = "vendor";
                                break;
                            default:
                                path = ".";
                                break;
                        }
                        path += autoload.path + "/";
                        path += className/*.substr(autoload.namespace.length)*/.replace(/[\\]/g, "\/") + ".php";
                        
                        candidates.push(path);
                    }
                }
                
                var not_checked = [];
                for (var i = 0; i < candidates.length; i++) {
                    not_checked.push(candidates[i]);
                }
                
                for (var i = 0; i < candidates.length; i++) {
                    if (result_sent) {
                        break;
                    }
                    candidate = candidates[i];
                    (function(candidate) {
                        fs.exists(candidate, function(result){
                            not_checked.splice(not_checked.indexOf(candidate), 1);
                            
                            if(result && !result_sent){
                                result_sent = true;
                                callback(candidate, candidates);
                            } else if(not_checked.length == 0) {
                                callback(null, candidates);
                            }
                        });
                    })(candidate);
                }
            });
        }
        
        function findService(id) {
            
        }
        
        function findRoute(name) {
            
        }
        
        /***** Lifecycle *****/
        
        plugin.on("load", function() {
            load();
        });
        plugin.on("unload", function() {

        });
        
        /***** Register and define API *****/
        
        plugin.freezePublicAPI({
            runSymfonyCommand: runSymfonyCommand,
            getCommandsList: getCommandsList,
            getContainer: getContainer,
            getRoutes: getRoutes,
            getTwigDirectives: getTwigDirectives,
            findNamespaces: findNamespaces,
            findClass: findClass,
            findService: findService,
            findRoute: findRoute
        });
        
        register(null, {
            "symfony_utils": plugin
        });
    }
});